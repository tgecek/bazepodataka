drop table KNJIGA;
drop table PISAC;
drop table IZDAVAC;



drop sequence PISAC_ID_SEQ;
drop sequence IZDAVAC_ID_SEQ;
drop sequence KNJIGA_ID_SEQ;


CREATE TABLE PISAC (
	id NUMBER(20) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	constraint PISAC_PK PRIMARY KEY (id));

CREATE sequence PISAC_ID_SEQ;

CREATE trigger BI_PISAC_ID
  before insert on PISAC
  for each row
begin
  select PISAC_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE KNJIGA (
	id NUMBER(10) NOT NULL,
	id_pisca NUMBER(20) NOT NULL,
	id_izdavaca NUMBER(20) NOT NULL,
	naslov VARCHAR2(50) NOT NULL,
	cijena NUMBER(20) NOT NULL,
	signatura NUMBER(20) NOT NULL,
	napomena VARCHAR2(20) NOT NULL,
	constraint KNJIGA_PK PRIMARY KEY (id));

CREATE sequence KNJIGA_ID_SEQ;

CREATE trigger BI_KNJIGA_ID
  before insert on KNJIGA
  for each row
begin
  select KNJIGA_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE IZDAVAC (
	id NUMBER(20) NOT NULL,
	ime_izdavaca VARCHAR2(20) NOT NULL,
	constraint IZDAVAC_PK PRIMARY KEY (id));

CREATE sequence IZDAVAC_ID_SEQ;

CREATE trigger BI_IZDAVAC_ID
  before insert on IZDAVAC
  for each row
begin
  select IZDAVAC_ID_SEQ.nextval into :NEW.id from dual;
end;

/


INSERT INTO PISAC (ime, prezime) VALUES
('Dante Alighieri', 'Alighieri');
INSERT INTO PISAC (ime, prezime) VALUES
('William Shakespeare', 'Shakespeare');
INSERT INTO PISAC (ime, prezime) VALUES
('Johann Wolfgang', 'Goethe');
INSERT INTO PISAC (ime, prezime) VALUES
('Fjodor Dostojevski', 'Dostojevski');
INSERT INTO PISAC (ime, prezime) VALUES
('Lav Tolstoj', 'Tolstoj');

INSERT INTO IZDAVAC (ime_izdavaca) VALUES ('Algoritam');

INSERT INTO IZDAVAC (ime_izdavaca) VALUES ('Profil');

INSERT INTO IZDAVAC (ime_izdavaca) VALUES ('Hena');

INSERT INTO IZDAVAC (ime_izdavaca) VALUES ('Naklada Ljevak');

INSERT INTO IZDAVAC (ime_izdavaca) VALUES ('Vulkan');



INSERT INTO KNJIGA (id_pisca, id_izdavaca, naslov, cijena, signatura, napomena) VALUES
(1, 1, 'Igra prijestolja', 100, 1, 'Op�irni opis knjige');

INSERT INTO KNJIGA (id_pisca, id_izdavaca, naslov, cijena, signatura, napomena) VALUES
(2, 2, 'Gospodar prstenova', 200, 2, 'Op�irni opis knjige');

INSERT INTO KNJIGA (id_pisca, id_izdavaca, naslov, cijena, signatura, napomena) VALUES
(3, 3, 'Rat i mir', 300, 3, 'Op�irni opis knjige');

INSERT INTO KNJIGA (id_pisca, id_izdavaca, naslov, cijena, signatura, napomena) VALUES
(4, 4, 'Doktor �ivago', 400, 4, 'Op�irni opis knjige');

INSERT INTO KNJIGA (id_pisca, id_izdavaca, naslov, cijena, signatura, napomena) VALUES
(5, 5, '1984', 500, 5, 'Op�irni opis knjige');

ALTER TABLE KNJIGA ADD CONSTRAINT KNJIGA_fk0 FOREIGN KEY (id_pisca) REFERENCES PISAC(id);
ALTER TABLE KNJIGA ADD CONSTRAINT KNJIGA_fk1 FOREIGN KEY (id_izdavaca) REFERENCES IZDAVAC(id);

select 
   k.id,
   p.prezime,
   p.ime,
   k.naslov,
   i.ime_izdavaca,
   k.cijena,
   k.signatura,
   k.napomena
   
from
   knjiga k,
   izdavac i,
   pisac p
where
   k.id_pisca = p.id and
   k.id_izdavaca = i.id 
select * from izdavac



select * from knjiga