drop table upisi;
drop table predmeti;
drop table nastavnici;
drop table studenti;

drop sequence STUDENTI_ID_SEQ;
drop sequence predmeti_ID_SEQ;
drop sequence nastavnici_ID_SEQ;
drop sequence upisi_ID_SEQ;



CREATE TABLE STUDENTI (
	ID NUMBER(9) NOT NULL,
	JMBAG NUMBER(8) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(20) UNIQUE NOT NULL,
	password VARCHAR2(10) NOT NULL,
	spol NUMBER(1) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (ID),
    CONSTRAINT ch_spol CHECK (spol IN (0, 1))
    );

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on STUDENTI
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE NASTAVNICI (
	ID NUMBER(9) NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1) NOT NULL,
	constraint NASTAVNICI_PK PRIMARY KEY (ID),
    constraint ch_spol1 CHECK (spol in (0,1))
    );
        
CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on NASTAVNICI
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE PREDMETI (
	ID NUMBER(9) NOT NULL,
	IDnastavnika NUMBER(9) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1) NOT NULL,
	semestar NUMBER(1) NOT NULL,
	constraint PREDMETI_PK PRIMARY KEY (ID),
    constraint ch_semestar CHECK (semestar in (1,2,3,4,5,6))
    );

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on PREDMETI
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE UPISI (
	ID NUMBER(9) NOT NULL,
	IDpredmeta NUMBER(9) NOT NULL,
	IDstudenta NUMBER(9) NOT NULL,
	datum DATE NOT NULL,
	ocjena NUMBER(1) NOT NULL,
	constraint UPISI_PK PRIMARY KEY (ID),
    constraint ch_ocjena CHECK (ocjena in (1,2,3,4,5) or ocjena is null)
    );
CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on UPISI
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


ALTER TABLE PREDMETI ADD CONSTRAINT PREDMETI_fk0 FOREIGN KEY (IDnastavnika) REFERENCES NASTAVNICI(ID);

ALTER TABLE UPISI ADD CONSTRAINT UPISI_fk0 FOREIGN KEY (IDpredmeta) REFERENCES PREDMETI(ID);
ALTER TABLE UPISI ADD CONSTRAINT UPISI_fk1 FOREIGN KEY (IDstudenta) REFERENCES STUDENTI(ID);

INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70147, 'Hrvoje', 'Horvat', 'h.horvat@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70148, 'Antun', 'Horvat', 'a.horvat@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70149, 'Stipe', 'Horvat', 's.horvat@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70150, 'Marko', 'Kova', 'm.kovac@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70151, 'Antun', 'Kova', 'a.kova@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70152, 'Ana', 'Vuk', 'a.vuk@gmail.com', 12345, 0);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70153, 'Izabela', 'Kovai', 'iza.kova@gmail.com', 12345, 0);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70154, 'Josip', 'Agi', 'j.agi@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70155, 'imun', 'Horvat', 's,horvat@gmail.com', 12345, 1);
INSERT into studenti (JMBAG, ime, prezime, email, password, spol) values (70156, 'Mate', 'Arambai', 'm.aram@gmail.com', 12345, 1);

INSERT into nastavnici (ime, prezime, email, password, spol) values ('Tena', 'Jankovi', 't.janko@gmail.com', 12345, 1);
INSERT into nastavnici (ime, prezime, email, password, spol) values ('Leona', 'Rai', 'l,raic@gmail.com', 12345, 0);
INSERT into nastavnici (ime, prezime, email, password, spol) values ('Nino', 'Dragovi', 'n.drago@gmail.com', 12345, 1);

INSERT into predmeti (idnastavnika, sifra, naziv, ects, semestar) values (1, 09876, 'IT i primjene', 5, 1);
INSERT into predmeti (idnastavnika, sifra, naziv, ects, semestar) values (2, 09877, 'Programiranje', 6, 2);
INSERT into predmeti (idnastavnika, sifra, naziv, ects, semestar) values (3, 09878, 'Mehatronika', 7, 3);

INSERT into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (1, 2, sysdate - 2, 4);
INSERT into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (2, 3, to_date('12-10-2023', 'DD-MM-YYYY'), 2);
INSERT into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (3, 2, sysdate - 2, 4);
INSERT into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (2, 2, sysdate - 2, 4);
INSERT into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (1, 1, sysdate - 2, 4);



select * from upisi



select to_date('12-10-2023', 'DD-MM-YYYY') from dual


select sysdate + 365 from dual


SELECT to_date('2023-12-10', 'YYYY-MM-DD') - to_date('2023-10-22', 'YYYY-MM-DD') AS days_difference
FROM dual;
