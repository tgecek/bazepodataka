drop table populacija_old
select * from populacija_old
update populacija_old set populacija=populacija/10
create table populacija (ID number, country varchar(255), populacija number, tip int DEFAULT(0), updated date, created date);
select * from populacija


create sequence POPULACIJA_ID_SEQ;


CREATE OR REPLACE TRIGGER BIU_POPULACIJA_ID 
BEFORE INSERT OR UPDATE ON POPULACIJA
for each row
BEGIN
  IF INSERTING THEN
     :NEW.id := populacija_ID_SEQ.nextval;
     :NEW.created := systimestamp;
  else
     :NEW.updated := systimestamp;
  end if;
END;

INSERT INTO populacija(country, populacija) select country, populacija from populacija_old
select * from populacija

drop table populacija_old

select * from populacija

update populacija 
set tip=1 where country in ('South America', 'Europe', 'North America', 'Asia ' || chr(38) || ' Oceania', 'Africa', 'Antarctica');

update populacija 
set tip=2 where country = 'World';


create or replace NONEDITIONABLE TRIGGER BIU_POPULACIJA_ID 
BEFORE INSERT OR UPDATE ON POPULACIJA
for each row
BEGIN
  IF INSERTING THEN
     :NEW.id := populacija_ID_SEQ.nextval;
     :NEW.created := systimestamp;
  else
     :NEW.updated := systimestamp;
  end if;
END;
