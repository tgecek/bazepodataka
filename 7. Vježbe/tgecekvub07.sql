drop table students;
drop table buses;

CREATE TABLE buses
( id number(6) NOT NULL PRIMARY KEY,
  reg_oznaka varchar2(10) NOT NULL,
  cijena number(5,2) NOT NULL,
  CONSTRAINT UQ_reg_oznaka UNIQUE (reg_oznaka)
);


CREATE TABLE students
( id number(6) NOT NULL PRIMARY KEY,
  JMBAG number(8) NOT NULL,
  autobus_id number(6),
  ime varchar2(50) NOT NULL,
  prezime varchar2(50) NOT NULL,
  e_mail  varchar2(50) NOT NULL,
  CONSTRAINT UQ_JMBAG UNIQUE (JMBAG),
  CONSTRAINT fk_autobus FOREIGN KEY(autobus_id)
  references buses(id)
);


insert into buses (ID, reg_oznaka, cijena) values (1, 'BJ 345 GH', 159.99);
insert into buses (ID, reg_oznaka, cijena) values (2, 'BJ 472 CD', 199.99);
insert into buses (ID, reg_oznaka, cijena) values (3, 'BJ 043 GB', 359.99);
insert into buses (ID, reg_oznaka, cijena) values (4, 'BJ 654 MK', 300.00);
insert into buses (ID, reg_oznaka, cijena) values (5, 'BJ 332 TS', 145.45);

insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (1, 36985478, 1, 'Mia', 'Horvat', 'mhorvat@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (2, 36985479, 1, 'Lucija', 'Kovačević', 'lkovacevic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (3, 36985480, 1, 'Ema', 'Babić', 'ebabic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (4, 36985481, 1, 'Ana', 'Marić', 'amaric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (5, 36985482, 1, 'Petra', 'Jurić', 'pjuric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (6, 36985483, 2, 'Lana', 'Novak', 'lnovak@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (7, 36985484, null, 'Dora', 'Kovačić', 'dkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (8, 36985485, 2, 'Marta', 'Vuković', 'mvukovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (9, 36985486, 2, 'Luka', 'Knežević', 'lknezevuc@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (10, 36985487, 2, 'Marko', 'Marković', 'mmarkovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (11, 36985488, null, 'Jakov', 'Petrović', 'jpetrovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (12, 36985489, null, 'Ivan', 'Matić', 'imatic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (13, 36985490, 3, 'Petar', 'Tomić', 'ptomic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (14, 36985491, 3, 'Matej', 'Kovač', 'mkovac@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (15, 36985492, 3, 'Filip', 'Pavlović', 'fpavlovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (16, 36985493, null, 'Mario', 'Perić', 'mperic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (17, 36985494, null, 'Anita', 'Jurković', 'ajurkovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (18, 36985495, null, 'Kazimir', 'Gelić', 'kgelic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (19, 36985496, null, 'Borna', 'Špiranec', 'spiranec@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (20, 36985497, null, 'Vesna', 'Molnar', 'vmolnar@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (21, 36985498, 2, 'Marija', 'Horvat', 'mhorvat1@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (22, 36985499, 2, 'Miroslav', 'Horvat', 'mhorvat2@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (23, 36985500, 3, 'Berislav', 'Marić', 'bmaric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (24, 36985501, 1, 'Jakov', 'Kovačić', 'jkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (25, 36985502, 1, 'Filip', 'Kovačić', 'fkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (26, 36985503, 1, 'Filip', 'Novak', 'fnovak@vub.hr');
commit;

select * from buses
select avg(cijena) from buses
select max(cijena) from buses
select min(cijena) from buses
select * from students where autobus_id is null
select count(1) from students where autobus_id is null
--8 studenta ne putuje
--7.
select 
    * 
from 
   students inner join buses on 
   students.autobus_id = buses.ID

--8.
select 
    * 
from 
   students inner join buses on 
   students.autobus_id = buses.ID
where reg_oznaka = 'BJ 345 GH'
--9.
select 
    * 
from 
   students right join buses on 
   students.autobus_id = buses.ID
--10.
select 
    * 
from 
   students full join buses on 
   students.autobus_id = buses.ID
--11.
select 
    * 
from 
   buses full join students on 
   buses.ID = students.autobus_id
--12.
select 
    prezime, count(1) 
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by prezime 
order by prezime asc
--13.
select 
    ime, prezime
from 
   students inner join buses on 
   students.autobus_id = buses.ID
order by prezime

--14.
select 
    prezime, ime
from 
   students inner join buses on 
   students.autobus_id = buses.ID
order by prezime, ime
--15.
select 
    ime, prezime
from 
   students inner join buses on 
   students.autobus_id = buses.ID
order by ime, prezime

--16. 
select 
    reg_oznaka, count(1)
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by reg_oznaka order by count(1) desc

--17.
select 
    reg_oznaka, cijena, count(1)
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by reg_oznaka, cijena order by count(1) desc

--18.
select 
    reg_oznaka, cijena, count(1)
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by reg_oznaka, cijena
having count(1) > 4
order by count(1) desc


--19.
select 
    reg_oznaka, cijena, count(1), cijena*count(1) as total
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by reg_oznaka, cijena
having count(1) > 4
order by count(1) desc

--20.
select 
    reg_oznaka, cijena, count(1), cijena*count(1) as total
from 
   students inner join buses on 
   students.autobus_id = buses.ID
group by reg_oznaka, cijena
having cijena*count(1) > 1200
order by count(1) desc






















select 
    * 
from 
   students right join buses on 
   students.autobus_id = buses.ID
where students.id is null
--14.
