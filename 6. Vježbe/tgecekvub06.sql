--1. zadatak
select 
    * 
from 
    common.naselja n, 
    common.zupanije z
where
    n.idzupanija = z.id

--2. zadatak
select 
    n.mjesto,
    n.opcina as op�ina,
    n.postanskibroj as po�ta,
    z.zupanija as �upanija
from 
    common.naselja n, 
    common.zupanije z
where
    n.idzupanija = z.id
--3. zadatak
select to_char(sysdate,'DD.MONTH.YYYY') as danas from dual;
--4. zadatak
select upper(substr(ime, 0, 1))||upper(substr(prezime, 0, 2)) as inicijali, ime, prezime from common.korisnici
--5. zadatak
select lower(substr(ime, 0, 1))|| lower(TRANSLATE(prezime,'�Ǝ���枚�','CCZSDcczsd')) as username, ime, prezime from common.korisnici
--6. zadatak

select 
    ime || ' ' || prezime as naziv, 
    0 || pozmob || ' ' || mobbro as mobitel, 
    CASE spol
    WHEN 0 THEN
    'mu�ko'
    WHEN 1 THEN
    '�ensko'
    else 
    'nepoznato'
    end as spol
from 
    common.korisnici k, 
    common.kontakti t
where
    t.idkorisnika=k.id

--7. zadatak
select distinct pozmob from common.kontakti

--8. zadatak 
select 
    ime || ' ' || prezime as naziv, 
    0 || pozmob || ' ' || mobbro as mobitel, 
    CASE spol
    WHEN 0 THEN
    'mu�ko'
    WHEN 1 THEN
    '�ensko'
    else 
    'nepoznato'
    end as spol,
    
    CASE pozmob
    WHEN 95 THEN
    'TELE2'
    WHEN 92 THEN
    'tomato'
    WHEN 97 THEN
    'bonbon'
    WHEN 91 THEN
    'A1'
    else 
    'nepoznato'
    end as operateri,
    
    pozmob as pozmob
from 
    common.korisnici k, 
    common.kontakti t
where
    t.idkorisnika=k.id

--9. zadatak

select 
    ime || ' ' || prezime as naziv, 
    0 || pozmob || ' ' || mobbro as mobitel, 
    CASE spol
    WHEN 0 THEN
    'mu�ko'
    WHEN 1 THEN
    '�ensko'
    else 
    'nepoznato'
    end as spol,
    
    CASE pozmob
    WHEN 95 THEN
    'TELE2'
    WHEN 92 THEN
    'tomato'
    WHEN 97 THEN
    'bonbon'
    WHEN 91 THEN
    'A1'
    else 
    'nepoznato'
    end as operateri,
    
    pozmob as pozmob
from 
    common.korisnici k, 
    common.kontakti t
where
    t.idkorisnika=k.id
and
    spol=0
and
    pozmob=95
--10.zadatak
select godiste,ime,prezime,mjesto,opcina from common.korisnici k, common.kontakti t, common.naselja n where t.idkorisnika=k.id and idzupanija=14
--11.zadatak
select (2023-godiste),ime,prezime,mjesto,opcina from common.korisnici k, common.kontakti t, common.naselja n where t.idkorisnika=k.id and idzupanija=14
--12.zadatak
select (2023-godiste),ime,prezime,mjesto,opcina from common.korisnici k, common.kontakti t, common.naselja n where t.idkorisnika=k.id and idzupanija=14 and (2023-godiste) between 19 and 23